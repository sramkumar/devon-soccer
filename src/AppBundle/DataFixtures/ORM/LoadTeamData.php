<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Team;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadTeamData.
 */
class LoadTeamData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getTeamData() as $data) {
            $team = new Team();
            $team->setName($data['name']);
            $team->setLogoUri($data['logoUri']);

            $manager->persist($team);

            if (false === array_key_exists('referenceName', $data)) {
                $data['referenceName'] = $data['name'].'-'.$data['version'];
            }

            $this->setReference($data['referenceName'], $team);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    private function getTeamData(): array
    {
        return [
            [
                'name' => 'Test Team1',
                'logoUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.1',
                'referenceName' => 'team-0.0.1',
            ],
            [
                'name' => 'Test Team2',
                'logoUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.2',
                'referenceName' => 'team-0.0.2',
            ],
            [
                'name' => 'Test Team3',
                'logoUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.3',
                'referenceName' => 'team-0.0.3',
            ],
        ];
    }
}
