<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Player;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPlayerData.
 */
class LoadPlayerData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getPlayerData() as $data) {
            $player = new Player();
            $player->setFirstName($data['firstName']);
            $player->setLastName($data['lastName']);
            $player->setImageUri($data['imageUri']);
            $player->setTeam($data['team']);

            $manager->persist($player);

            if (false === array_key_exists('referenceName', $data)) {
                $data['referenceName'] = $data['firstName'].'-'.$data['version'];
            }

            $this->setReference($data['referenceName'], $player);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 2;
    }

    /**
     * @return array
     */
    private function getPlayerData(): array
    {
        return [
            [
                'firstName' => 'Test Player1',
                'lastName' => 'Test Player1',
                'imageUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.1',
                'referenceName' => 'player-0.0.1',
                'team' => $this->getReference('team-0.0.1'),
            ],
            [
                'firstName' => 'Test Player2',
                'lastName' => 'Test Player2',
                'imageUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.2',
                'referenceName' => 'player-0.0.2',
                'team' => $this->getReference('team-0.0.2'),
            ],
            [
                'firstName' => 'Test Player3',
                'lastName' => 'Test Player3',
                'imageUri' => '1906f65c0af24953b0ef2b130bf7b06e.jpeg',
                'version' => '0.0.3',
                'referenceName' => 'player-0.0.3',
                'team' => $this->getReference('team-0.0.2'),
            ],
        ];
    }
}
