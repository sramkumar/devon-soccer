<?php


namespace AppBundle\Service;


use AppBundle\Repository\PlayerRepository;
use AppBundle\Repository\TeamRepository;
use Symfony\Component\Form\FormInterface;

class TeamService
{
    /** @var TeamRepository  */
    protected  $teamRepository;

    /** @var PlayerRepository */
    protected  $playerRepository;

    public function __construct(
        TeamRepository $teamRepository,
        PlayerRepository $playerRepository
    )
    {
        $this->teamRepository = $teamRepository;
        $this->playerRepository = $playerRepository;
    }

    /**
     * Get all teams and order by name
     * @return string
     */
    public function getAllTeams()
    {
        return $this->teamRepository->findAllTeams();
    }

    /**
     * Get all players in a team and order by first name
     * @return string
     */
    public function getAllPlayers($team)
    {
        return $this->playerRepository->findAllPlayersByTeam($team);
    }

    /**
     * Remove the team based on Id
     * @param $teamId
     */
    public function removeTeam($teamId)
    {
        $team = $this->teamRepository->find($teamId);
        $this->teamRepository->remove($team);
    }

    /**
     * Persist a team
     * @param $team
     */
    public function insertTeam($team)
    {
        $this->teamRepository->persist($team);
    }

    /**
     * Get form errors in a human readable format
     * @param FormInterface $form
     * @return array
     */
    public function getFormErrors(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrors($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}