<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Team;
use Doctrine\ORM\EntityRepository;

/**
 * Class TeamRepository.
 */
class TeamRepository extends EntityRepository
{
    /**
     * Get all teams
     */
    public function findAllTeams()
    {
        return $this->findAll();
    }

    public function persist(Team $team)
    {
        $this->_em->persist($team);
        $this->_em->flush();
    }


    public function remove(Team $team)
    {
        $this->_em->remove($team);
        $this->_em->flush();
    }
}
