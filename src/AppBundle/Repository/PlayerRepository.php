<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class TeamRepository.
 */
class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    /**
     * Find all players by team id
     * @param $team
     */
    public function findAllPlayersByTeam($team)
    {
        $sql = 'SELECT p FROM AppBundle:Player p
        WHERE p.team = :team
        ORDER BY p.firstName DESC';

        $parameters = [
            'team' => $team
        ];

        $result = $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters($parameters)
            ->setCacheable(true)
            ->getResult()
        ;

        return $result;
    }

    /**
     * @param Player $player
     */
    public function persist(Player $player)
    {
        $this->getEntityManager()->persist($player);
    }
}
