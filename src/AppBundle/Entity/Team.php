<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="logo_uri", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank(message="Logo URI should not be empty.")
     * @Assert\Url(
     *    message = "The logo uri '{{ value }}' is not a valid url",
     * )
     */
    private $logoUri;


    /**
     * @var ArrayCollection|Player[]
     *
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team", orphanRemoval=true, cascade={
     *   "persist", "refresh", "remove"  })
     */
    private $players;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logoUri
     *
     * @param string $logoUri
     * @return Team
     */
    public function setLogoUri($logoUri)
    {
        $this->logoUri = $logoUri;

        return $this;
    }

    /**
     * Get logoUri
     *
     * @return string
     */
    public function getLogoUri()
    {
        return $this->logoUri;
    }


    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return Team
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->players[] = $player;
        $player->setTeam($this);
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
}
