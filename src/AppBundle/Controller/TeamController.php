<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Team;
use AppBundle\Form\TeamType;
use AppBundle\Repository\TeamRepository;
use AppBundle\Service\TeamService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class TeamController extends AbstractFOSRestController
{
    /** @var TeamRepository */
    protected  $teamRepository;

    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * Post Team and its players data
     * @FOS\Route("/team", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function postTeamsAction(Request $request)
    {
        $team = new Team();
        $form = $this->createForm(TeamType::class, $team, array('method' => 'POST'));
        $parameters = $request->request->all();
        $form->submit($parameters);

        if ($form->isValid()) {
            $this->teamRepository->persist($team);
            return View::create()->setStatusCode(Response::HTTP_ACCEPTED)->setData('Team & Players created successfully');
        }else{
            $errors = $this->teamService->getFormErrors($form);
            $data = [
                'type' => 'validation_error',
                'title' => 'There was a validation error',
                'errors' => $errors
            ];
            return View::create()->setStatusCode(Response::HTTP_BAD_REQUEST)->setData($data);
        }
    }


    /**
     * Get all teams
     * @FOS\Route("/team", methods={"GET"})
     * @return Response
     */
    public function getTeamsAction()
    {
        $teams = $this->teamRepository->findAllTeams();
        return View::create()->setStatusCode(Response::HTTP_ACCEPTED)->setData($teams);
    }

    /**
     * Delete a particular team
     * @FOS\Route("/team", methods={"DELETE"})
     * @return Response
     */
    public function deleteTeamAction(Request $request)
    {
        $teamId = $request->get('teamId');
        try {
            $this->teamRepository->remove($teamId);
            return View::create()->setStatusCode(Response::HTTP_ACCEPTED)->setData('Team Deleted Successfully');
        } catch (\Error $e) {
            return View::create()->setStatusCode(Response::HTTP_BAD_REQUEST)->setData('Invalid Team Id');

        }
    }
}