<?php


namespace AppBundle\Controller;

use AppBundle\Service\TeamService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class PlayerController extends AbstractFOSRestController
{
    /** @var TeamService */
    protected  $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    /**
     * Get all the players in a particular team
     * @FOS\Route(
     *      "/players/{teamId}",
     *      requirements={"id" = "\d+"},
     *      name="get_players",
     *      methods={"GET"}
     * )
     * @param $teamId
     * @return JsonResponse
     */
    public function getPlayersAction($teamId)
    {
        $players = $this->teamService->getAllPlayers($teamId);
        return View::create()->setStatusCode(Response::HTTP_ACCEPTED)->setData($players);
    }
}