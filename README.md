soccer
======

A Symfony project created on April 2, 2019, 10:37 am.

clone the project and then

Do a composer install
```
php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate

php bin/console server:start 127.0.0.1:8000    -   To Start the server without virtual host (then go to http://127.0.0.1:8000
```

>Api Route to get all teams
```
127.0.0.1:8000/api/get/teams
```
>Api Route to get players of a team
```
127.0.0.1:8000/api/get/players/{teamId} 
{teamId} is an integer
```
>Api Route to delete a team
```
127.0.0.1:8000/api/delete/team/{teamId}

```
>Api Route to create a team
```
127.0.0.1:8000/api/post/team
```
Sample Format to post data
```
{
	"name": "test",
	"logoUri": "www.google.com/images",
	"players": [
		{ "firstName": "test",
		  "lastName": "test123",
		  "imageUri": "http://www.google.com"
		},
		{ "firstName": "test2",
		  "lastName": "test1234",
		  "imageUri": "http://www.google.com"
		}
	]
}
```