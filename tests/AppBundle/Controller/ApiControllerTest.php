<?php

namespace Tests\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\AppBundle\FixturedTestCase;

class ApiControllerTest extends FixturedTestCase
{
    /**
     * Tests gets collection.
     */
    public function testGetTeamsAction()
    {
        /** @var Team[] $expected */
        $expected = [
            // In this particular order due to the Repository ORDER BY field.
            $this->referenceRepository->getReference('team-0.0.3'),
            $this->referenceRepository->getReference('team-0.0.2'),
            $this->referenceRepository->getReference('team-0.0.1'),
        ];

        $this->client->request('GET', '/api/team');
        $response = $this->client->getResponse();
        $this->isSuccessful($response);
        $response = json_decode($response->getContent(), true);

        $this->assertEquals(count($expected), count($response));

        foreach ($response as $actualTeam) {
            $currentExpected = array_shift($expected);

            $this->assertEquals($currentExpected->getName(), $actualTeam['name']);
        }
    }

    /**
     * Tests gets collection.
     */
    public function testPlayersForTeamAction()
    {
        /** @var Players[] $expected */
        $expected = [
            // In this particular order due to the Repository ORDER BY field.
            $this->referenceRepository->getReference('player-0.0.3'),
            $this->referenceRepository->getReference('player-0.0.2'),
        ];

        $team = $this->referenceRepository->getReference('team-0.0.2');

        $this->client->request('GET', '/api/players/'.$team->getId());
        $response = $this->client->getResponse();
        $this->isSuccessful($response);
        $response = json_decode($response->getContent(), true);

        $this->assertEquals(count($expected), count($response));

        foreach ($response as $actualTeam) {
            $currentExpected = array_shift($expected);

            $this->assertEquals($currentExpected->getFirstName(), $actualTeam['first_name']);
        }
    }

    /**
     * Tests team name cannot be empty
     */
    public function testPostTeamNotFound()
    {
        $this->client->request('POST', '/api/team', ['name' => 'test']);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    /**
     * Test that a proper team is created.
     */
    public function testPostTeamWithNoException()
    {
        $payload = [
                "name" => "test",
                "logoUri" => "www.google.com/images",
                "players" => [
                    [ "firstName" => "test",
                      "lastName" => "test123",
                      "imageUri"=> "http://www.google.com"
                    ],
                    [ "firstName"=> "test2",
                      "lastName"=> "test1234",
                      "imageUri"=> "http://www.google.com"
                    ]
                ]
        ];

        $this->client->request(
            'POST',
            '/api/team',
            $payload
        );
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());
        $this->assertEquals('"Team & Players created successfully"', $response->getContent());
    }

    /**
     * Tests throws 404 exception when deleting non-existing.
     */
    public function testDeleteTeamNotFound()
    {
        $this->client->request('DELETE', '/api/team/18');
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    /**
     * Tests deleting an existing team
     */
    public function testDeleteTeam()
    {
        $this->client->request('DELETE', '/api/team/1');
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_ACCEPTED, $response->getStatusCode());
    }
}
