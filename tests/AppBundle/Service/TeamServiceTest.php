<?php

namespace Tests\AppBundle\Services;

use AppBundle\Repository\PlayerRepository;
use AppBundle\Repository\TeamRepository;
use AppBundle\Service\TeamService;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\AppBundle\FixturedTestCase;

/**
 * Class TeamServiceTest.
 */
class TeamServiceTest extends FixturedTestCase
{
    /**
     * @var TeamService
     */
    private $teamService;

    /**
     * @var TeamRepository|\Mockery\Mock
     */
    private $teamRepository;

    /**
     * @var PlayerRepository|\Mockery\Mock
     */
    private $playerRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->teamRepository = \Mockery::mock(TeamRepository::class);
        $this->playerRepository = \Mockery::mock(PlayerRepository::class);
        $this->teamService = new TeamService($this->teamRepository, $this->playerRepository);
    }

    /**
     * Tests returns a response object by branch and name.
     */
    public function testGetAllTeams()
    {
        /** @var Teams[] $expected */
        $teams = new ArrayCollection([
            // In this particular order due to the Repository ORDER BY field.
            $this->referenceRepository->getReference('team-0.0.3'),
            $this->referenceRepository->getReference('team-0.0.2'),
            $this->referenceRepository->getReference('team-0.0.1'),
        ]);

        $this->teamRepository
            ->shouldReceive('findAllTeams')
            ->withAnyArgs()
            ->andReturn($teams)
        ;

        $this->assertEquals($teams, $this->teamService->getAllTeams());
    }

    protected function tearDown(): void
    {
        \Mockery::close();

        parent::tearDown();
    }
}
