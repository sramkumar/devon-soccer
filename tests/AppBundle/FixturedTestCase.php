<?php

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Client;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class FixturedTestCase.
 */
class FixturedTestCase extends WebTestCase
{
    /**
     * @var ReferenceRepository
     */
    protected $referenceRepository;

    /**
     * @var Client
     */
    protected $client;

    protected function setUp()
    {
        $fixtures = [
            'AppBundle\DataFixtures\ORM\LoadTeamData',
            'AppBundle\DataFixtures\ORM\LoadPlayerData',
        ];

        $this->referenceRepository = $this->loadFixtures($fixtures)->getReferenceRepository();
        $this->client = static::createClient();
    }
}
